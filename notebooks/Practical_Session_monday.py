# -*- coding: utf-8 -*-
"""
Created on Mon Jun 12 11:49:27 2017

@author: Mossa
"""



import numpy as np

from matplotlib import pyplot as plt

def translation_scaling_invariance(data):
    """ remove translation and scaling from d x k data """

    #step one: center data    
    center_of_mass = np.mean(data, 0)
    centered_data = data - center_of_mass[np.newaxis,]
    
    #step two: scaling
    data_frobenius_norm = np.linalg.norm(centered_data, 'fro')    
    
    centered_and_scaled_data = centered_data/data_frobenius_norm
    
    return centered_and_scaled_data


def orthogonal_procrustes_analysis(data_A, data_B):
    """ """
    
    #step one: translation and scaling
    centered_and_scaled_data_A = translation_scaling_invariance(data_A)
    centered_and_scaled_data_B = translation_scaling_invariance(data_B)
    
    #remove rotation
    
    u,s,v = np.linalg.svd(centered_and_scaled_data_B.dot(centered_and_scaled_data_A.T))
    
    R_star = u.dot(v)

    
    #check determinant?
    if not np.sign(np.linalg.det(R_star)):
        raise ValueError("The determinant is negative!?")
        
    #let B be transformation invariant to A and return both!
    result_A = centered_and_scaled_data_A
    result_B = (R_star.T).dot(centered_and_scaled_data_B) #insanity??!
    
    
    return result_A, result_B

def Log_map_Kendall_space( p, q):
    
    p = p.flatten()
    q = q.flatten()
    
    if np.linalg.norm(p-q) <1e-7:
        raise ValueError("The provided vectors are (almost) equal...")
    
    theta = np.arccos(np.inner(p,q))

    u = lambda q: (q - np.inner(p,q) * p) /np.linalg.norm(q - np.inner(p,q) * p)
    
    gamma = lambda t: np.cos(t*theta)*p + np.sin(t*theta) * u(q)
    
    Log_p = lambda q: theta * u(q)
    
    return(Log_p(q), gamma)

if __name__ == "__main__":
    
#==============================================================================
#     
#==============================================================================
    
    data = np.genfromtxt("./cc-shapes/cc.00.lpts" )

    
#==============================================================================
#     
#==============================================================================
    
    #load all the shapes
    import glob
    path_list_all_shapes = glob.glob(".\\cc-shapes\\cc.*.lpts")
    
    all_shapes_data = []
    for f in path_list_all_shapes:
        all_shapes_data.append(np.genfromtxt(f))
    np.array(all_shapes_data)
    
#==============================================================================
#     
#==============================================================================
    
    #plot the raw data
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(data[:,0],data[:,1], 'xb',label="raw data")
    
    plt.legend(loc='best');

    #scaled thing
    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    
    processed_data = translation_scaling_invariance(data);
    ax.plot(processed_data[:,0],processed_data[:,1], 
             'ok',label="centered data and scaled")
    
    ax.legend(loc="best")
    
#==============================================================================
#     
#==============================================================================
    fig  = plt.figure()
    ax = fig.add_subplot(111)
    
    A = all_shapes_data[1]
    B = all_shapes_data[30]
    
    A_new, B_new = orthogonal_procrustes_analysis(A,B)
    
    A = translation_scaling_invariance(A)
    B = translation_scaling_invariance(B)
    
    ax.plot(A[:,0],A[:,1], linewidth=None,marker="o",color="green", label="A")
    ax.plot(B[:,0],B[:,1], linewidth=None,marker="o",color="black", label="B")
    ax.plot(A_new[:,0],A_new[:,1],linewidth=None, marker=".",color="green",label="inv A")
    ax.plot(B_new[:,0],B_new[:,1], linewidth=0  ,marker=".", color="black",label="inv B")
    
    ax.legend(loc="best")
    
#==============================================================================
#     
#==============================================================================
    
    log_map_AB,gamma_AB = Log_map_Kendall_space(A_new,B_new)
    log_map_AB.shape = (64,2)
    
    
    fig  = plt.figure()
    ax = fig.add_subplot(111)
    
    ax.plot(A[:,0],A[:,1], 
                 '.')
    ax.plot(B[:,0],B[:,1], 
                 '.')
    ax.plot(log_map_AB[:,0],log_map_AB[:,1], 
                 'ok')
    
    ax.legend(["A","B","Log AB"])
    ax.legend(loc="best")
