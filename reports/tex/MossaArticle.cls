% !TeX root = main.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%------ Identification -------%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{MossaArticle}[17/02/14 Mossa's template for school]
\LoadClass[12pt]{article}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%------- Class Options -------%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\DeclareOption{Nomencl}{\def\@nomencl{}} %Option acted upon at bottom.
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions\relax

\ifdefined\@nomencl
    \RequirePackage{nomencl}
    \setlength{\nomitemsep}{-\parsep}
    \renewcommand*{\pagedeclaration}[1]{\unskip\dotfill\hyperpage{#1}}
    \makenomenclature
    %In CMD, makeindex main.nlo -s nomencl.ist -o main.nls   If the filename is not "main", just replace it.
    \renewcommand{\nomname}{List of Symbols}
\else\fi

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%----- Required Packages -----%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% geometry  : Document dimensions
% amsfonts  : Math symbols.
% amsmath   : Math formulas.
% bm        : It provides bold math symbols.
% booktabs  : Features for tables.
% graphicx  : Manages inclusion of graphic files (better than graphics)
% setspace  : Manages spacing in the document.
% float     : Put [H] after \begin{figure} to force position of figure.
% sectsty   : Used to have hline under new section.
% mypage    : title page customized by Selim. use \mytitlepage.
% grffile   : allows spaces in file names.
% enumitem  : reduce space in lists. \begin{itemize}[nolistsep]
% subcaption: Subfigures.
% multicol  : For use with Excel2Latex
% bigstrut  : Same as above.
% pdfpages  : To use pdfpages, do \includepdf{document}
% url       : Allows for \url{}. Useful for bibliographies with url.
% hyperref  : Hyperlinks. Use \hypersetup{hidelinks} for no borders.
% cleveref  : can use \cref or \Cref to reference
% nomencl   : List of symbols -> \nomenclature{$symbol$}{description}

\RequirePackage{nag}

%\RequirePackage[english]{babel} %conflict with KU-forside

\usepackage[T1]{fontenc}

\RequirePackage{xparse}
\RequirePackage{geometry}
\RequirePackage{amsfonts, amsmath, bm}
\RequirePackage{iftex}

\RequirePackage[standard,hyperref,amsmath]{ntheorem}

\RequirePackage{booktabs}
\ifPDFTeX
	\RequirePackage[pdftex]{graphicx}
	
	\RequirePackage[utf8]{inputenc}         % æ,ø,å m.m.
\ifluatex
	%suggestion: use graphicx with lualatex
	\RequirePackage[luatex]{graphicx}
\fi
\RequirePackage{setspace}
\RequirePackage{float}
\RequirePackage{sectsty}
\RequirePackage[space]{grffile}
\RequirePackage[shortlabels]{enumitem}
\RequirePackage{subcaption}

\RequirePackage{bigstrut}
\RequirePackage{multicol}
\RequirePackage{multirow}
\RequirePackage{pdfpages}
\RequirePackage{url}
\RequirePackage{hyperref}
\DeclareTextCommandDefault{\textbullet}{\ensuremath{\bullet}}
\RequirePackage[noabbrev]{cleveref}

\RequirePackage[allowlitunits]{siunitx}
\RequirePackage{microtype}
\RequirePackage{commath}
\RequirePackage{array}
\RequirePackage{bm}

\RequirePackage{epstopdf}
%\RequirePackage{autonum} %TODO: ues etextools, which conflicts with lualatex
\RequirePackage{pgf, tikz}
\usepackage{tikz-cd} % added in order to use quotes tikz-library

\RequirePackage{algpseudocode}
\RequirePackage{algorithm}
\RequirePackage{aliascnt}
\RequirePackage[textwidth=\marginparwidth]{todonotes}
\RequirePackage{marginfix}

\RequirePackage{marginnote}
\RequirePackage{ragged2e}     % Justering af elementer

\RequirePackage{datetime} % Dato og tid

\RequirePackage{lastpage} % Giver mulighed for at referere til sidste side


\RequirePackage{lmodern} % Pænere danske bogstaver
%\RequirePackage{enumerate} % Til lister med tal

\RequirePackage{longtable} % Tabeller over flere sider
\RequirePackage{dcolumn}              

\RequirePackage[nottoc]{tocbibind}
\RequirePackage[footnote, danish]{fixme}
\RequirePackage{enumitem}

\RequirePackage{xfrac}

\RequirePackage{microtype}

\RequirePackage{snapshot}

%cleveref works well? scrap this
%s\newcommand{\thmautorefname}{Sætning}
%\providecommand*{\exampleautorefname}{Example}
%\providecommand*{\remarkautorefname}{Remark}

\renewcommand{\epsilon}{\varepsilon}
%\crefname{thm}{S\ae tning}{S\ae tninger}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\urlstyle{same} % Ændre udseende af links


% Kode blok
\usepackage{listings}
\lstset{framexleftmargin=5mm, 
	extendedchars=true, 
	basicstyle={\ttfamily\tiny}, 
	keywordstyle={\ttfamily\tiny}, 
	columns=flexible, 
	numbers=left, 
	numberstyle={\ttfamily\tiny}, breaklines, 
	showstringspaces=false, language=C}

\lstset{breaklines=true}

\newcommand{\celcius}{$^{\circ}$C}
\newcommand{\kelvin}{$^{\circ}$K}

%\newcommand{\blankpage}{\clearpage\newpage\thispagestyle{empty}\textcolor{white}{.}\newpage} % Laver blank side med \blankpage

%\newcommand{\litteratur}{\blankpage\bibliography{kilder}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%BRUGERMAKROER%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\newcommand{\Z}[0]{\mathbb Z}
\newcommand{\N}[0]{\mathbb N}
\newcommand{\R}[0]{\mathbb R}
\newcommand{\Q}[0]{\mathbb Q}
\newcommand{\C}[0]{\mathbb C}

\newcommand{\grad}[1]{\mathrm{grad}#1}
\newcommand{\floor}[1]{\left\lfloor #1 \right\rfloor}
\newcommand{\ceil}[1]{\left\lceil #1 \right\rceil}
\newcommand{\supp}[1]{\mathrm{supp}\,#1}
\newcommand{\spann}[1]{\mathrm{span}\,\set{#1}}
\newcommand{\ess}[1]{\mathrm{ess}\,#1}
\renewcommand{\vec}[1]{\bm{#1}}
\newcommand{\mat}[1]{\bm{#1}}
\newcommand{\tran}{^{\mathstrut\scriptscriptstyle\top}} 
\newcommand{\genericdel}[4]{%
	\ifcase#3\relax
	#1#4#2\or
	\bigl#1#4\bigr#2\or
	\Bigl#1#4\Bigr#2\or
	\biggl#1#4\biggr#2\or
	\Biggl#1#4\Biggr#2\else
	\left#1#4\right#2\fi
}
\renewcommand{\eval}[2][-1]{\genericdel.|{#1}{#2}}

\newcommand{\logit}[1]{\mathrm{logit}#1}

\newcommand{\EE}{{\rm{E}}}
\def\Var{{\mathrm{ Var}}\,}
\def\Cov{{\mathrm{ Cov}}\,}

\newcommand{\BB}{{\m{B}}}
\newcommand{\mX}{\mathcal{X}}
\newcommand{\mY}{\mathcal{Y}}
\renewcommand{\exp}[1]{ \ensuremath{\mathrm{\operatorname{e}}^{ #1} } }
\newcommand{\eexp}[1]{ \ensuremath{\mathrm{\operatorname{exp}}\!\left(#1\right)}}
\renewcommand{\log}[1]{\ensuremath{\mathrm{\operatorname{log}}\!\left(#1\right)}}

\newcommand{\Exp}[2]{ \ensuremath{\mathrm{\operatorname{Exp}}_{#1} \!\left(#2\right)}}
\newcommand{\Log}[2]{\ensuremath{\mathrm{\operatorname{Log}}_{#1}  \!\left(#2\right)}}


\newcommand{\Perp}{\perp \! \! \! \perp}

\newcommand{\nn}[1]{n_\text{#1}(i)}

\newcommand{\pp}[1]{\hat p_\text{#1}(i)}
\newcommand{\setG}[1]{\left(\set{\text{#1}},E_{\set{\text{#1}}}\right)}	
\newcommand{\graphmodel}[1]{\left(G_{\text{#1}},{\bar{\mathcal{P}}}_{\text{#1}}\right)}	


\renewcommand{\Pr}[1]{\ensuremath{\mathrm {\operatorname{P}}\!\left(#1\right)}}

%source: http://tex.stackexchange.com/questions/187162/vertical-bar-for-absolute-value-and-conditional-expectation
\usepackage{mathtools}
\newcommand{\expect}{\operatorname{E}\expectarg}
\DeclarePairedDelimiterX{\expectarg}[1]{(}{)}{%
	\ifnum\currentgrouptype=16 \else\begingroup\fi
	\activatebar#1
	\ifnum\currentgrouptype=16 \else\endgroup\fi
}

\newcommand{\innermid}{\nonscript\;\delimsize\vert\nonscript\;}
\newcommand{\activatebar}{%
	\begingroup\lccode`\~=`\|
	\lowercase{\endgroup\let~}\innermid 
	\mathcode`|=\string"8000
}

%source: http://tex.stackexchange.com/questions/290275/mathematical-notation-of-convergence-in-latex
%Convergence arrows
\newcommand{\myrightarrow}[1]{\xrightarrow{\makebox[1em][c]{$\scriptstyle#1$}}}

\newcommand{\convas}{\myrightarrow{\text{a.s.}}}
\newcommand{\convd}{\myrightarrow{\text{\mathrm D}}}
\newcommand{\convp}{\myrightarrow{\text{P}}}
\newcommand{\convL}{\myrightarrow{L^1}}
\newcommand{\convLp}{\myrightarrow{L^p}}

%source: http://tex.stackexchange.com/questions/26637/how-do-you-get-mathbb1-to-work-characteristic-function-of-a-set
\newcommand{\indicator}[1]{\rm{1}_{\left(#1\right)}}
\DeclareMathOperator*{\argmin}{argmin}
\DeclareMathOperator*{\argmax}{argmax}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%---------- Options ----------%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\geometry{a4paper,hmargin={2cm, 5.5cm}, vmargin={2.5cm, 2.5cm}, marginparwidth=5 cm}
%\geometry{a4paper}
\geometry{a4paper,left=2cm,right=2cm,top=2.5cm,bottom=2.5cm, headheight=25pt}

%\Crefname{equation}{Eq.}{Eqs.}
%\setstretch{1.5}
\frenchspacing
\setlength{\skip\footins}{1cm}

